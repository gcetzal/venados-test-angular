const PROXY_CONFIG = {
    "/api": {
      "target": "https://venados.dacodes.mx",
      "secure": false,
      "pathRewrite": {
        "^/api": ""
      },
      "bypass": function (req, res, proxyOptions) {
              if (req.headers.accept.indexOf("html") !== -1) {
                  console.log("Skipping proxy for browser request.");
                  return "/index.html";
              }
              req.headers["X-Custom-Header"] = "yes";
          }
    }
  };
  
  module.exports = PROXY_CONFIG;