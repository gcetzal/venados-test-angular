import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { StatisticComponent } from './pages/statistic/statistic.component';
import { PlayersComponent } from './pages/players/players.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'statistic', component: StatisticComponent },
  { path: 'players', component: PlayersComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
