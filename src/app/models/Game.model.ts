export class Game {
    constructor(
        public local: string,
        public opponent: string,
        public opponent_image: string,
        public datetime:string,
        public league:string,
        public image:string,
        public home_score: number,
        public away_score: number
    ){}
}