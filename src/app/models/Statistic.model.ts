export class Statistic {
    constructor(
        public birth_place?: string,
        public birthday?: string,
        public first_surname?: string,
        public height?: number,
        public image?: string,
        public last_team?: string,
        public name?: string,
        public number?: number,
        public position?: string,
        public position_short?: string,
        public second_surname?: string,
        public weight?: string

    ) {}
}