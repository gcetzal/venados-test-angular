import { TestBed } from '@angular/core/testing';

import { VenadosService } from './venados.service';

describe('VenadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VenadosService = TestBed.get(VenadosService);
    expect(service).toBeTruthy();
  });
});
