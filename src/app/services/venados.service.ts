import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VenadosService {

  headers: HttpHeaders;

  constructor(public http: HttpClient) { 
    this.headers = new HttpHeaders().set('Accept', 'application/json');
  }

  loadStatistic() {
    let url = '/api/statistics';
    
    return this.http.get(url, {headers: this.headers}).pipe(map((res:any) => {
      return res.data.statistics
    }));
  }

  loadPlayers() {
    let url = '/api/players';

    return this.http.get(url, {headers: this.headers}).pipe(map((res:any) => {
      return res.data.team;
    }));
  }

  loadGames() {
    let url = '/api/games';

    return this.http.get(url, {headers: this.headers}).pipe(map((res:any) => {
      return res.data.games
    }));
  }
}
