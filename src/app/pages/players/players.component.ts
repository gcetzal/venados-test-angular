import { Component, OnInit } from '@angular/core';
import { VenadosService } from 'src/app/services/venados.service';
import { Player } from 'src/app/models/Player.model';
import { MatDialog } from '@angular/material';
import { ModalComponent } from 'src/app/components/modal/modal.component';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  playerForward: Player[] = [];
  playerCenter: Player[] = [];
  playerDefense: Player[] = [];
  playerGoalkeeper: Player[] = [];

  constructor(public _venadosService: VenadosService, public dialog: MatDialog) { }

  ngOnInit() {
    this.loadPlayers()
  }

  loadPlayers(): void {
    this._venadosService.loadPlayers().subscribe((res) => {
      this.playerForward = res.forwards
      this.playerCenter = res.centers
      this.playerDefense = res.defenses
      this.playerGoalkeeper = res.goalkeepers

      
    })
  }

  openDialog(player): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      panelClass: 'custom-dialog-container',
      width: '400px',
      data: player
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
    });
  }

}


