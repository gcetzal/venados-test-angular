import { Component, OnInit } from '@angular/core';
import { VenadosService } from 'src/app/services/venados.service';
import { Statistic } from 'src/app/models/Statistic.model';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {

  statistics: Statistic[] = [];

  constructor(public _venadosService: VenadosService) { }

  ngOnInit() {
    this.loadDataStatistic();
    console.log(this.statistics)
  }

  loadDataStatistic() {
    this._venadosService.loadStatistic().subscribe(resp => {
      this.statistics = resp
    });
  }

}
