import { Component, OnInit } from '@angular/core';
import { VenadosService } from 'src/app/services/venados.service';
import { Game } from 'src/app/models/Game.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data: Game[] = [];
  filterGame: any;

  constructor(public _venadosService: VenadosService) { }

  ngOnInit() {
    this.loadData()
  }

  loadData(): void {
    this._venadosService.loadGames().subscribe((res: any) => {
      this.data = res;
      this.filterData();
    })
  }

  filterData(type: string = 'Ascenso MX') {
    let filterData = this.data.filter((item) => item.league == type);

    this.groubByMonth(filterData);

  }

  groubByMonth(filterData) {
    const groups = filterData.reduce((groups, game) => {
      const date = game.datetime.split('T')[0].substr(0, 7);
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(game);
      return groups;
    }, {});

    const groupArrays = Object.keys(groups).map((date) => {
      return {
        date,
        games: groups[date]
      };
    });
    this.filterGame = groupArrays;
  }

}
